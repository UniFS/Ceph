# Ceph Encryption:

OSD:
- https://docs.ceph.com/docs/master/ceph-volume/lvm/encryption/
- https://github.com/maricaantonacci/ceph-tutorial/wiki/OSD-Encryption

Rook
- https://github.com/rook/rook/issues/2926

FS:
- https://tracker.ceph.com/projects/ceph/wiki/Cephfs_encryption_support

Object:
- https://docs.ceph.com/docs/master/radosgw/encryption/

RBD
- https://specs.openstack.org/openstack/cinder-specs/specs/queens/rbd-encryption.html

Guide:
- https://www.reddit.com/r/ceph/comments/91n4s6/understanding_securely_implementing_encryption/
- https://www.stratoscale.com/blog/openstack/openstack-security-storage-setup-cloud-part-2/

Encryption at Rest
- https://docs.openstack.org/project-deploy-guide/charm-deployment-guide/latest/app-encryption-at-rest.html