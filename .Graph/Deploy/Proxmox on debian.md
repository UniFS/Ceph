# Guide
- https://pve.proxmox.com/wiki/Ceph_Server
- https://pve.proxmox.com/wiki/Full_Mesh_Network_for_Ceph_Server"


# Discussion
https://www.reddit.com/r/homelab/comments/7be88s/why_i_think_ceph_is_an_improvement_over_zfs_for/dphb6cu/?context=8&depth=9
> [parent](https://www.reddit.com/r/homelab/comments/7be88s/why_i_think_ceph_is_an_improvement_over_zfs_for/)

Quote: "How have you deployed Ceph in your homelab?
I suspect the answer is: Proxmox."


